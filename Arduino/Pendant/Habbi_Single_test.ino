//pin order left to right: {PIN_PC0, PIN_PA5, PIN_PA3, PIN_PA4, PIN_PB1, PIN_PB0]};
//rev. 2 pin order left to right: {PIN_PB0, PIN_PA3, PIN_PB2, PIN_PB1, PIN_PA5, PIN_PA4};
//pin array stores the pins in order right to left
#include <avr/sleep.h>

int pinArray[6] = {PIN_PB0, PIN_PA3, PIN_PB2, PIN_PB1, PIN_PA5, PIN_PA4};

void RTC_init(void)
{
  /* Initialize RTC: */
  while (RTC.STATUS > 0)
  {
    ;                                   /* Wait for all register to be synchronized */
  }
  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;    /* 32.768kHz Internal Ultra-Low-Power Oscillator (OSCULP32K) */

  RTC.PITINTCTRL = RTC_PI_bm;           /* PIT Interrupt: enabled */

  RTC.PITCTRLA = RTC_PERIOD_CYC32768_gc /* RTC Clock Cycles 16384, resulting in 32.768kHz/16384 = 2Hz */
  | RTC_PITEN_bm;                       /* Enable PIT counter: enabled */
}

ISR(RTC_PIT_vect)
{
  RTC.PITINTFLAGS = RTC_PI_bm;          /* Clear interrupt flag by writing '1' (required) */
}
void setup() {
  digitalWrite(PIN_PB7, HIGH);
  RTC_init();                           /* Initialize the RTC timer */
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  /* Set sleep mode to POWER DOWN mode */
  sleep_enable();
}

void loop() {
//give some breathing room at start
 
//iterate through pin array, fading each pin in
 for (int p = 6; p > 0; p--) {
  fadeIn(pinArray[p]);
 }
 
//give some breathing room at end of fade in line
sleep_cpu();

//iterate through pin array, fading each pin out
for (int p = 0; p < 6; p++) {
  fadeOut(pinArray[p]);
 }
 
//give some breathing room at end of fade out line
sleep_cpu();
 
}

//functions for fade in/fade out from arduino examples.
//fade value ranges from 0 to 255, change values for max/min brightness
//fade value affects granularity of pwm change
void fadeIn(int pin){
  for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 5) {
    analogWrite(pin, fadeValue);
    delay(10);
  }
 }
 
void fadeOut(int pin) {
    for (int fadeValue = 255 ; fadeValue >= 0; fadeValue -= 5) {
    analogWrite(pin, fadeValue);
    delay(10);
  }
 }
