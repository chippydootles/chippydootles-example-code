#include <FastLED.h>

#define ENPIN    PIN_PB7
#define GREENPIN PIN_PB1
#define BLUEPIN  PIN_PB2
#define REDPIN   PIN_PB0

void showAnalogRGB( const CRGB& rgb)
{
  analogWrite(REDPIN,   rgb.r );
  analogWrite(GREENPIN, rgb.g );
  analogWrite(BLUEPIN,  rgb.b );
}

void colorBars()
{
  showAnalogRGB( CRGB::Red );   delay(500);
  showAnalogRGB( CRGB::Green ); delay(500);
  showAnalogRGB( CRGB::Blue );  delay(500);
  showAnalogRGB( CRGB::Black ); delay(500);
}


void setup() {
  pinMode(REDPIN,   OUTPUT);
  pinMode(GREENPIN, OUTPUT);
  pinMode(BLUEPIN,  OUTPUT);
  pinMode(ENPIN,    OUTPUT);

  digitalWrite(ENPIN, HIGH);

  // Flash the "hello" color sequence: R, G, B, black.
  colorBars();
}

void loop() {
  // put your main code here, to run repeatedly:
  static uint8_t hue;
  hue = hue + 1;
  // Use FastLED automatic HSV->RGB conversion
  showAnalogRGB( CHSV( hue, 255, 255) );
  
  delay(20);
}
