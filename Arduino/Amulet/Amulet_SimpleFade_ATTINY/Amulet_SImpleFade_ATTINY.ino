/*This simple sketch is intended for use on the anwaar amulet with Attiny
1616 mcu. If programing for the amulet make sure you have the megaTinyCore installed.
to upload to your attiny based amulet make sure programmer is serial udpi, 
the corresponding port is selected, and the amulet switched on*/

#include <avr/sleep.h>

//array of unconnected pins
int unconnectedPins[10] = {PIN_PA0, PIN_PA1, PIN_PA2, PIN_A6, PIN_PB3, PIN_PB4, PIN_PB5, PIN_PC1, PIN_PC2, PIN_PC3}; 

//array of pwm pins
int pinArray[6] = {PIN_PB0, PIN_PB2, PIN_PB1, PIN_PA3, PIN_PA4, PIN_PA5};

//Voltage Select Pin
int vSel = PIN_PA7;

//Analog in pin connected to ven, do not drive this pin
int aVcap = PIN_PC0;

//rtc set up
void RTC_init(void)
{
  /* Initialize RTC: */
  while (RTC.STATUS > 0)
  {
    ;                                   /* Wait for all register to be synchronized */
  }
  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;    /* 32.768kHz Internal Ultra-Low-Power Oscillator (OSCULP32K) */

  RTC.PITINTCTRL = RTC_PI_bm;           /* PIT Interrupt: enabled */

  RTC.PITCTRLA = RTC_PERIOD_CYC32768_gc /* RTC Clock Cycles 16384, resulting in 32.768kHz/16384 = 2Hz */
  | RTC_PITEN_bm;                       /* Enable PIT counter: enabled */
}

ISR(RTC_PIT_vect)
{
  RTC.PITINTFLAGS = RTC_PI_bm;          /* Clear interrupt flag by writing '1' (required) */
}


void setup() {
  
  //set up sleep
  RTC_init();                       /* Initialize the RTC timer */
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  /* SLEEP_MODE_IDLE, SLEEP_MODE_STANDBY, SLEEP_MODE_PWR_DOWN )  */
  sleep_enable(); 

  //set up pins
  pinMode(vSel, OUTPUT); /* voltage toggle pin*/
  pinMode(aVcap, INPUT); /*set as input or leave floating */

  //Pull UDPI low
  digitalWrite(PIN_PA0, LOW);
  
  //set all pwm pins as output
  for (int p=0; p<3; p++) {
    pinMode(pinArray[p], OUTPUT);
  }
  
  // set unconnected pins output for power savings
  for (int p=0; p<10; p++) {
    pinMode(unconnectedPins[p], OUTPUT);
  }
}

void loop() {
  fadeIn();
  sleep_cpu();
  fadeOut();
  sleep_cpu();
  sleep_cpu();
  sleep_cpu();
  
}

/* fade functions, sets the top/bottom duty cycle and 
iterates in steps of the fadevalue. Uncomment/comment out 
needed pins according to faceplate*/
 void fadeIn(){
  for (int fadeValue = 0 ; fadeValue <= 254; fadeValue += 2) {
    analogWrite(pinArray[0], fadeValue);
    analogWrite(pinArray[1], fadeValue);
    analogWrite(pinArray[2], fadeValue);
    //analogWrite(pinArray[3], fadeValue);
    //analogWrite(pinArray[4], fadeValue); 
    //analogWrite(pinArray[5], fadeValue);
    delay(10);
  }
}
 
void fadeOut(){
    for (int fadeValue = 254 ; fadeValue >= 0; fadeValue -= 2) {
    analogWrite(pinArray[0], fadeValue);
    analogWrite(pinArray[1], fadeValue);
    analogWrite(pinArray[2], fadeValue);
    //analogWrite(pinArray[3], fadeValue); 
    //analogWrite(pinArray[4], fadeValue); 
    //analogWrite(pinArray[5], fadeValue);  
    delay(10);
  }
}
