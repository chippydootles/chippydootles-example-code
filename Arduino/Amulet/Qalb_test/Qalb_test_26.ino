#include <avr/sleep.h>

int pinArray[3] = { PIN_PA5,PIN_PA4, PIN_PA7 };


void RTC_init(void)
{
  /* Initialize RTC: */
  while (RTC.STATUS > 0)
  {
    ;                                   /* Wait for all register to be synchronized */
  }
  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;    /* 32.768kHz Internal Ultra-Low-Power Oscillator (OSCULP32K) */

  RTC.PITINTCTRL = RTC_PI_bm;           /* PIT Interrupt: enabled */

  RTC.PITCTRLA = RTC_PERIOD_CYC16384_gc /* RTC Clock Cycles 16384, resulting in 32.768kHz/16384 = 2Hz */
  | RTC_PITEN_bm;                       /* Enable PIT counter: enabled */
}

ISR(RTC_PIT_vect)
{
  RTC.PITINTFLAGS = RTC_PI_bm;          /* Clear interrupt flag by writing '1' (required) */
}

void setup() {
  RTC_init();                           /* Initialize the RTC timer */
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  /* Set sleep mode to POWER DOWN mode */
  sleep_enable();
}

void loop() {

PulseUp(pinArray[1]);
PulseDown(pinArray[0]);
sleep_cpu();

PulseDown(pinArray[1]);
PulseUp(pinArray[0]);
sleep_cpu();
}

void PulseUp(int pin){
  for (int fadeValue = 0 ; fadeValue <= 75; fadeValue += 25) {
    analogWrite(pin, fadeValue);
    delay(10);
  }
  for (int fadeValue = 0 ; fadeValue <= 200; fadeValue += 10) {
    analogWrite(pin, fadeValue);
    delay(10);
  }
 }
 
void PulseDown(int pin) {
    for (int fadeValue = 200 ; fadeValue >= 0; fadeValue -= 10) {
    analogWrite(pin, fadeValue);
    delay(10);
  }
 }
