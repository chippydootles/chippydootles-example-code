# Chippydootles Example Code
This repository is for code examples for various chippydootles boards.

More to come as I refine and test different sketches.

For arduino make sure you have the proper cores installed according to boards
MCU and any libraries mentioned being needed by the sketch. Coding is tested on arduino 1.8.19 and has not been tested in the newer 2.0 versions of the arduino IDE. You can try them but your mileage may vary.